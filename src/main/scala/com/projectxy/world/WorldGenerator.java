package com.projectxy.world;

import com.projectxy.Blocks$;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.IChunkGenerator;
import net.minecraft.world.gen.feature.WorldGenMinable;
import net.minecraftforge.fml.common.IWorldGenerator;

import java.util.Random;

public class WorldGenerator implements IWorldGenerator {
    private static int maxHeight = 60, minHeight = 20;
    static WorldGenMinable[] mineable = new WorldGenMinable[] {
            new WorldGenMinable(Blocks$.MODULE$.blockOre().getStateFromMeta(0), 7),
            new WorldGenMinable(Blocks$.MODULE$.blockOre().getStateFromMeta(1), 7),
            new WorldGenMinable(Blocks$.MODULE$.blockOre().getStateFromMeta(2), 7),
            new WorldGenMinable(Blocks$.MODULE$.blockOre().getStateFromMeta(3), 7),
            new WorldGenMinable(Blocks$.MODULE$.blockOre().getStateFromMeta(4), 7),
    };
    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        if (world.provider.getDimension() == 0) {
            for (int i = 0; i < 12; i++) {
                int y = minHeight + random.nextInt(maxHeight - minHeight);
                int x = chunkX * 16 + random.nextInt(16);
                int z = chunkZ * 16 + random.nextInt(16);
                mineable[random.nextInt(5)].generate(world, random, new BlockPos(x, y, z));
            }
        }
    }
}
