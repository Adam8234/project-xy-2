package com.projectxy

import codechicken.lib.colour.ColourRGBA
import codechicken.lib.packet.ICustomPacketHandler.{IClientPacketHandler, IServerPacketHandler}
import codechicken.lib.packet.PacketCustom
import com.projectxy.tile.TileXyLight
import net.minecraft.client.Minecraft
import net.minecraft.entity.player.EntityPlayerMP
import net.minecraft.network.play.{INetHandlerPlayClient, INetHandlerPlayServer}

object ProjectXYCPH extends IClientPacketHandler {
  override def handlePacket(packetCustom: PacketCustom, mc: Minecraft, handler: INetHandlerPlayClient): Unit = {
    packetCustom.getType match {
      case 1 =>
        mc.world.getTileEntity(packetCustom.readPos()).asInstanceOf[TileXyLight].handleUpdatePacket(packetCustom)
    }
  }
}

object ProjectXYSPH extends IServerPacketHandler {
  override def handlePacket(packetCustom: PacketCustom, sender: EntityPlayerMP, handler: INetHandlerPlayServer): Unit = {
  }
}
