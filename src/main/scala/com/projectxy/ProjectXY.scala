package com.projectxy

import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.ItemStack
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.Mod.EventHandler
import net.minecraftforge.fml.common.event.{FMLInitializationEvent, FMLPostInitializationEvent, FMLPreInitializationEvent}
import net.minecraftforge.fml.common.network.NetworkRegistry

/**
  * Created by Acorp on 6/17/2017.
  */
@Mod(modid = "projectxy", modLanguage = "scala")
object ProjectXY {
  val MOD_ID = "projectxy"

  @EventHandler def init(event: FMLInitializationEvent) {
    ProjectXYProxy.init()
  }

  @EventHandler def preInit(event: FMLPreInitializationEvent) {
    ProjectXYProxy.preInit()
  }

  @EventHandler def postInit(event: FMLPostInitializationEvent) {
    ProjectXYProxy.postInit()
  }
}

object ProjectXYBlocksTab extends CreativeTabs("projectxy.tabBlocks") {
  override def getTabIconItem: ItemStack = new ItemStack(Blocks.blockOre, 1, 2)
}
