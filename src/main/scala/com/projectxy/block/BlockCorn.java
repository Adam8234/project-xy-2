package com.projectxy.block;

import com.projectxy.Items$;
import net.minecraft.block.BlockCrops;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;

import java.util.Random;

public class BlockCorn extends BlockCrops {
    public BlockCorn() {
        setRegistryName("corn_crop");
        setUnlocalizedName("corn_crop");
        setTickRandomly(true);
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        int age = getAge(state);
        float hMin = 0.0F;
        float hMax = 1.0F;
        float wMin = 0.1F;
        float wMax = 0.9F;
        switch (age) {
            case 0:
            case 1:
                return new AxisAlignedBB(wMin, hMin, wMin, wMax, hMax - 0.8F, wMax);
            case 2:
            case 3:
                return new AxisAlignedBB(wMin, hMin, wMin, wMax, hMax - 0.4F, wMax);
            case 4:
            case 5:
            default:
                return new AxisAlignedBB(wMin, hMin, wMin, wMax, hMax, wMax);
        }
    }

    @Override
    public int getMaxAge() {
        return 5;
    }

    @Override
    public EnumPlantType getPlantType(IBlockAccess world, BlockPos pos) {
        return EnumPlantType.Crop;
    }

    @Override
    public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand) {
        if (isFullyGrown(worldIn, pos)) {
            return;
        }
        if (worldIn.getLightFromNeighbors(pos.up()) >= 9) {
            int age = getAge(state);
            float f = getGrowthChance(this, worldIn, pos);
            int k = rand.nextInt((int) (25.0F / f) + 1);
            if ((age < 4) && (k == 0)) {
                worldIn.setBlockState(pos, withAge(++age));
                if (age == 4) {
                    worldIn.setBlockState(pos.up(), withAge(5));
                }
            }
        }
    }


    @Override
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
        super.breakBlock(worldIn, pos, state);
        if (getAge(state) == 5) {
            dropBlockAsItem(worldIn, pos.down(), withAge(4), 1);
            worldIn.setBlockToAir(pos.down());
        }
    }

    @Override
    public void grow(World worldIn, BlockPos pos, IBlockState state) {
        if (worldIn.isAirBlock(pos.up()) && !isFullyGrown(worldIn, pos)) {
            worldIn.setBlockState(pos, withAge(4));
            worldIn.setBlockState(pos.up(), withAge(5));
        }
    }

    public boolean isFullyGrown(World world, BlockPos pos) {
        IBlockState state = world.getBlockState(pos);
        int age = getAge(state);
        return state.getBlock() == this && (age == 4 || age == 5);
    }

    @Override
    public boolean canBlockStay(World worldIn, BlockPos pos, IBlockState state) {
        IBlockState belowState = worldIn.getBlockState(pos.down());
        if (belowState.getBlock().canSustainPlant(belowState, worldIn, pos.down(), EnumFacing.UP, this) || belowState.getBlock() == this) {
            return belowState.getLightValue(worldIn, pos) > 8 || worldIn.canBlockSeeSky(pos);
        }
        return false;
    }

    @Override
    protected Item getSeed() {
        return Items$.MODULE$.itemCornKernel();
    }

    @Override
    protected Item getCrop() {
        return Items$.MODULE$.itemCorn();
    }
}
