package com.projectxy.block;

import codechicken.lib.model.bakery.ModelBakery;
import codechicken.lib.texture.IWorldBlockTextureProvider;
import codechicken.lib.texture.TextureUtils;
import com.projectxy.ProjectXY$;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

public class BlockLegacy extends BlockXY implements TextureUtils.IIconRegister, IWorldBlockTextureProvider {
    private String textureName;
    private TextureAtlasSprite texture;

    public BlockLegacy(Material material) {
        super(material);
        TextureUtils.addIconRegister(this);
        ModelBakery.registerBlockKeyGenerator(this, ModelBakery.defaultBlockKeyGenerator);
    }

    public void setTextureName(String textureName) {
        this.textureName = textureName;
    }

    @Override
    public void registerIcons(TextureMap textureMap) {
        texture = textureMap.registerSprite(new ResourceLocation(ProjectXY$.MODULE$.MOD_ID(), "blocks/" + textureName));
    }

    @Override
    public TextureAtlasSprite getTexture(EnumFacing side, IBlockState state, BlockRenderLayer layer, IBlockAccess world, BlockPos pos) {
        return texture;
    }

    @Override
    public TextureAtlasSprite getTexture(EnumFacing side, ItemStack stack) {
        return texture;
    }
}
