package com.projectxy.block
import net.minecraft.block.state.IBlockState
import net.minecraft.item.ItemStack
import net.minecraft.util.math.BlockPos
import net.minecraft.world.IBlockAccess

trait IAnimationColorMultiplier extends IAnimationHandler {
  override def getColorMultiplier(world: IBlockAccess, state: IBlockState, pos: BlockPos, side: Int) = getAnimationColor(world, state, pos, side)

  override def getColorMultiplier(stack: ItemStack, side: Int) = getAnimationColor(stack, side)
}
