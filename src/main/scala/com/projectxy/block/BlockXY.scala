package com.projectxy.block

import com.projectxy.ProjectXYBlocksTab
import net.minecraft.block.Block
import net.minecraft.block.material.Material

class BlockXY(material: Material = Material.ROCK) extends Block(material) {
  setCreativeTab(ProjectXYBlocksTab)

  def setBlockName(name: String): BlockXY = {
    setRegistryName(name)
    setUnlocalizedName(s"blockXy${name.capitalize}")
    this
  }
}
