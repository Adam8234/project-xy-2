package com.projectxy.block;

import com.projectxy.Blocks$;
import com.projectxy.ProjectXYBlocksTab$;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.NonNullList;

public class BlockStone extends Block {
    private static PropertyEnum<BlockStone.Type> VARIANT = PropertyEnum.create("type", BlockStone.Type.class);

    public BlockStone() {
        super(Material.ROCK);
        setUnlocalizedName("blockXyStone");
        setRegistryName("stone");
        setDefaultState(blockState.getBaseState().withProperty(VARIANT, Type.STONE));
        setCreativeTab(ProjectXYBlocksTab$.MODULE$);
        setResistance(1.0f);
        setHardness(1.0f);
    }

    public enum Type implements IStringSerializable {
        STONE,
        STONE_BRICK,
        STONE_BRICK_FANCY,
        STONE_WHITE,
        STONE_WHITE_BRICK,
        STONE_WHITE_BRICK_FANCY;

        @Override
        public String getName() {
            return name().toLowerCase();
        }

        public int meta() {
            return ordinal();
        }

        @Override
        public String toString() {
            return name().toLowerCase();
        }

        public ItemStack toItemStack() {
            return new ItemStack(Blocks$.MODULE$.blockStone(), 1, meta());
        }
    }

    @Override
    public BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, VARIANT);
    }

    @SuppressWarnings("deprecation")
    @Override
    public IBlockState getStateFromMeta(int meta) {
        return getDefaultState().withProperty(VARIANT, Type.values()[meta]);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(VARIANT).meta();
    }

    @Override
    public void getSubBlocks(CreativeTabs tab, NonNullList<ItemStack> list) {
        for (int i = 0; i < Type.values().length; i++) {
            list.add(new ItemStack(this, 1, i));
        }
    }

    @Override
    public int damageDropped(IBlockState state) {
        return getMetaFromState(state);
    }
}
