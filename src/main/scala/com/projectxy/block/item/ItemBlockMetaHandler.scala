package com.projectxy.block.item

import net.minecraft.block.Block
import net.minecraft.item.{ItemBlock, ItemStack}

class ItemBlockMetaHandler(block: Block) extends ItemBlock(block) {
  setRegistryName(block.getRegistryName)
  setHasSubtypes(true)
  setMaxDamage(0)

  override def getMetadata(meta: Int): Int = meta
  override def getUnlocalizedName(stack: ItemStack): String = s"${super.getUnlocalizedName(stack)}|${stack.getItemDamage}"
}
