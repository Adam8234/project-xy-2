package com.projectxy.block

import java.util

import codechicken.lib.colour.{Colour, EnumColour}
import com.projectxy.item.ItemXychorium
import com.projectxy.tile.TileXyLight
import com.projectxy.{Blocks, Items, ProjectXYProxy}
import net.minecraft.block.ITileEntityProvider
import net.minecraft.block.material.Material
import net.minecraft.block.properties.{IProperty, PropertyInteger}
import net.minecraft.block.state.{BlockStateContainer, IBlockState}
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.{Item, ItemStack}
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.math.{BlockPos, RayTraceResult}
import net.minecraft.util.{EnumBlockRenderType, EnumFacing, EnumHand, NonNullList}
import net.minecraft.world.{IBlockAccess, World}

abstract class BlockGlow(val colors: List[Colour], material: Material = Material.IRON) extends MetaBlock(material) with IAnimationHandler {
  override def getAnimationColor(world: IBlockAccess, state: IBlockState, pos: BlockPos, side: Int): Int = colors(getMetaFromState(state)).rgba()

  override def getAnimationColor(stack: ItemStack, side: Int): Int = colors(stack.getMetadata).rgba()

  override def getSubBlocks(itemIn: CreativeTabs, items: NonNullList[ItemStack]): Unit = {
    for (meta <- colors.indices) {
      items.add(new ItemStack(this, 1, meta))
    }
  }
}

class BlockOre extends BlockGlowTexture("ore", Blocks.xyColors) {
  setHardness(3.0f)
  setResistance(5.0f)
  val max = 5
  val min = 3

  override def getDrops(world: IBlockAccess, pos: BlockPos, state: IBlockState, fortune: Int): util.List[ItemStack] = {
    val count: Int = min + world.asInstanceOf[World].rand.nextInt(max - min)
    dropXpOnBlockBreak(world.asInstanceOf[World], pos, min + world.asInstanceOf[World].rand.nextInt(5 - 2))
    val list = new util.ArrayList[ItemStack]()
    list.add(new ItemStack(Items.itemXychorium, count, getMetaFromState(state)))
    list
  }
}

class BlockGlowTexture(override val textureName: String, colors: List[Colour],
                       material: Material = Material.IRON) extends BlockGlow(colors, material) with ITextureRegisteringAnimationHandler {
  setBlockName(textureName)
  setHardness(2.0f)
  setResistance(5.0f)
}

class MetaBlock(material: Material = Material.IRON) extends BlockXY(material) {
  setDefaultState(getBlockState.getBaseState.withProperty(MetaBlock.META.asInstanceOf[IProperty[Integer]], 0.asInstanceOf[Integer]))

  override def createBlockState(): BlockStateContainer = new BlockStateContainer(this, MetaBlock.META)

  override def getStateFromMeta(meta: Int): IBlockState = getDefaultState.withProperty(MetaBlock.META.asInstanceOf[IProperty[Integer]], meta.asInstanceOf[Integer])

  override def getMetaFromState(state: IBlockState): Int = state.getValue(MetaBlock.META)

  override def getPickBlock(state: IBlockState, target: RayTraceResult, world: World, pos: BlockPos, player: EntityPlayer): ItemStack = new ItemStack(this, 1, getMetaFromState(state))

  override def damageDropped(state: IBlockState): Int = getMetaFromState(state)
}

class BlockXyLight() extends MetaBlock(Material.IRON) with ITextureRegisteringAnimationHandler with ITileEntityProvider {
  override val textureName: String = "light"

  override def getAnimationColor(world: IBlockAccess, state: IBlockState, pos: BlockPos, side: Int): Int = {
    val tile = world.getTileEntity(pos)
    if (tile != null && tile.isInstanceOf[TileXyLight]) {
      return tile.asInstanceOf[TileXyLight].getColour.rgba()
    } else {
      EnumColour.WHITE.rgba()
    }
  }

  override def getAnimationColor(stack: ItemStack, side: Int): Int = EnumColour.WHITE.rgba()

  override def getRenderType(state: IBlockState): EnumBlockRenderType = ProjectXYProxy.glowSimpleRenderType

  override def onBlockActivated(worldIn: World, pos: BlockPos, state: IBlockState, playerIn: EntityPlayer, hand: EnumHand, facing: EnumFacing, hitX: Float, hitY: Float, hitZ: Float): Boolean = {
    val item = playerIn.getHeldItem(hand)
    val tile = worldIn.getTileEntity(pos).asInstanceOf[TileXyLight]


    if (item.getItem.isInstanceOf[ItemXychorium]) {
      val colour = tile.getColour
      var r: Int = colour.r
      var g: Int = colour.g
      var b: Int = colour.b

      def add(value: Int): Int = {
        if (playerIn.isSneaking) {
          if (value > 0)
            return value - 1
        } else {
          if (value < 255)
            return value + 1
        }
        value
      }

      item.getMetadata match {
        case 0 => r = add(r)
        case 1 => g = add(g)
        case 2 => b = add(b)
      }

      tile.setColor(r, g, b)
      tile.markDirty()
      tile.sendUpdatePacket()

      return true
    }

    super.onBlockActivated(worldIn, pos, state, playerIn, hand, facing, hitX, hitY, hitZ)
  }

  override def createNewTileEntity(worldIn: World, meta: Int): TileEntity = new TileXyLight
}

object MetaBlock {
  val META: PropertyInteger = PropertyInteger.create("type", 0, 15)
}




