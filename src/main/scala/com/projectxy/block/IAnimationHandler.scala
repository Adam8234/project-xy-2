package com.projectxy.block

import codechicken.lib.colour.{Colour, ColourRGBA}
import com.projectxy.ProjectXYProxy
import net.minecraft.block.Block
import net.minecraft.block.state.IBlockState
import net.minecraft.client.renderer.texture.TextureAtlasSprite
import net.minecraft.entity.EntityLiving
import net.minecraft.item.ItemStack
import net.minecraft.util.{BlockRenderLayer, EnumBlockRenderType}
import net.minecraft.util.math.BlockPos
import net.minecraft.world.IBlockAccess
import net.minecraftforge.fml.relauncher.{Side, SideOnly}

/**
  * Created by Acorp on 6/17/2017.
  */
trait IAnimationHandler extends Block {
  def getAnimationColor(world: IBlockAccess, state: IBlockState, pos: BlockPos, side: Int): Int

  def getAnimationColor(stack: ItemStack, side: Int): Int
  @SideOnly(Side.CLIENT)
  def getAnimationTexture(world: IBlockAccess, state: IBlockState, pos: BlockPos, side: Int): TextureAtlasSprite = ProjectXYProxy.animationTexture.texture
  @SideOnly(Side.CLIENT)
  def getAnimationTexture(stack: ItemStack, side: Int): TextureAtlasSprite = ProjectXYProxy.animationTexture.texture
  @SideOnly(Side.CLIENT)
  def getOverlayTexture(world: IBlockAccess, state: IBlockState, pos: BlockPos, side: Int): TextureAtlasSprite
  @SideOnly(Side.CLIENT)
  def getOverlayTexture(stack: ItemStack, side: Int): TextureAtlasSprite

  def getColorMultiplier(world: IBlockAccess, state: IBlockState, pos: BlockPos, side: Int): Int = new ColourRGBA(1.0, 1.0, 1.0, 1.0).rgba()

  def getColorMultiplier(stack: ItemStack, side: Int): Int = new ColourRGBA(1.0, 1.0, 1.0, 1.0).rgba()

  def getAnimationBrightness(world: IBlockAccess, state: IBlockState, pos: BlockPos, side: Int): Int = 220

  def getAnimationBrightness(stack: ItemStack, side: Int): Int = 220

  override def getRenderType(state: IBlockState): EnumBlockRenderType = ProjectXYProxy.glowSimpleRenderType

  override def canCreatureSpawn(state: IBlockState, world: IBlockAccess, pos: BlockPos, `type`: EntityLiving.SpawnPlacementType): Boolean = false

  override def canRenderInLayer(state: IBlockState, layer: BlockRenderLayer): Boolean = layer == BlockRenderLayer.CUTOUT_MIPPED || layer == BlockRenderLayer.SOLID
}
