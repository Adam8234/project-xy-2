package com.projectxy.block

import codechicken.lib.texture.TextureUtils
import codechicken.lib.texture.TextureUtils.IIconRegister
import codechicken.lib.util.CommonUtils
import com.projectxy.ProjectXY
import net.minecraft.block.state.IBlockState
import net.minecraft.client.renderer.texture.{TextureAtlasSprite, TextureMap}
import net.minecraft.item.ItemStack
import net.minecraft.util.ResourceLocation
import net.minecraft.util.math.BlockPos
import net.minecraft.world.IBlockAccess
import net.minecraftforge.fml.relauncher.{Side, SideOnly}

/**
  * Created by Acorp on 6/17/2017.
  */
trait ITextureRegisteringAnimationHandler extends IAnimationHandler with IIconRegister {
  if(CommonUtils.isClient)
    TextureUtils.addIconRegister(this)

  var texture: TextureAtlasSprite = _
  val textureName: String

  @SideOnly(Side.CLIENT)
  override def registerIcons(textureMap: TextureMap): Unit = {
    texture = textureMap.registerSprite(new ResourceLocation(ProjectXY.MOD_ID, s"blocks/overlay/$textureName"))
  }

  @SideOnly(Side.CLIENT)
  override def getOverlayTexture(world: IBlockAccess, state: IBlockState, pos: BlockPos, side: Int): TextureAtlasSprite = texture
  @SideOnly(Side.CLIENT)
  override def getOverlayTexture(stack: ItemStack, side: Int): TextureAtlasSprite = texture
}
