package com.projectxy

import com.projectxy.item._
import net.minecraft.item.Item
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.event.RegistryEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent

object Items {
  lazy val itemXychoridite = new ItemXychoridite(Blocks.xyColors)
  lazy val itemXychorium = new ItemXychorium(Blocks.xyColors)
  lazy val itemCorn = new ItemXy("corn")
  lazy val itemCornCob = new ItemXyFood("corn_cob", 6)
  lazy val itemPopcorn = new ItemXyFood("popcorn", 3)
  lazy val itemCornKernel = new ItemKernel()

  def init(): Unit = {
    MinecraftForge.EVENT_BUS.register(this)
  }

  @SubscribeEvent
  def registerAllItems(event: RegistryEvent.Register[Item]): Unit = {
    val registry = event.getRegistry
    registry.register(itemXychoridite)
    registry.register(itemXychorium)
    registry.register(itemCorn)
    registry.registerAll(itemCornCob, itemPopcorn, itemCornKernel)
  }
}
