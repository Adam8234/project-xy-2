package com.projectxy

import codechicken.lib.render.block.BlockRenderingRegistry
import com.projectxy.client.{AnimationFX, CustomModelRegistry}
import com.projectxy.item.TColoredItem
import com.projectxy.tile.TileXyLight
import com.projectxy.world.WorldGenerator
import net.minecraft.client.Minecraft
import net.minecraft.client.renderer.color.IItemColor
import net.minecraft.item.ItemStack
import net.minecraftforge.fml.common.registry.GameRegistry
import net.minecraftforge.fml.relauncher.{Side, SideOnly}

class Proxy {
  def preInit(): Unit = {
    Blocks.init()
    Items.init()
  }

  def init(): Unit = {
    Recipies.init()
    GameRegistry.registerWorldGenerator(new WorldGenerator(), 0)
    GameRegistry.registerTileEntity(classOf[TileXyLight], "xy_light")
  }

  def postInit(): Unit = {

  }
}

class ClientProxy extends Proxy {
  lazy val glowSimpleRenderType = BlockRenderingRegistry.createRenderType("projectxy_isbr_glow_simple")
  lazy val animationTexture = new AnimationFX

  @SideOnly(Side.CLIENT)
  override def preInit(): Unit = {
    super.preInit()
    animationTexture.texture
    ModelHandler.init()
    BlockRenderingRegistry.registerRenderer(glowSimpleRenderType, CustomModelRegistry.glowSimpleRenderer)
  }

  @SideOnly(Side.CLIENT)
  override def init(): Unit = {
    super.init()
    Minecraft.getMinecraft.getItemColors.registerItemColorHandler(new IItemColor {
      override def getColorFromItemstack(stack: ItemStack, tintIndex: Int): Int = stack.getItem.asInstanceOf[TColoredItem].colorMultiplier(stack).rgb()
    }, Items.itemXychorium, Items.itemXychoridite)
  }

  @SideOnly(Side.CLIENT)
  override def postInit(): Unit = {
    super.postInit()
  }
}

object ProjectXYProxy extends ClientProxy