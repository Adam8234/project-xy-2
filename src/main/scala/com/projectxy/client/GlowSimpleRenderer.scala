package com.projectxy.client

import codechicken.lib.render.buffer.BakingVertexBuffer
import codechicken.lib.render.{CCModel, CCRenderState}
import codechicken.lib.vec.uv.IconTransformation
import codechicken.lib.vec.{Cuboid6, Translation, Vector3}
import com.projectxy.block.IAnimationHandler
import net.minecraft.block.Block
import net.minecraft.block.state.IBlockState
import net.minecraft.client.renderer._
import net.minecraft.client.renderer.texture.TextureAtlasSprite
import net.minecraft.client.renderer.vertex.DefaultVertexFormats
import net.minecraft.item.ItemStack
import net.minecraft.util.BlockRenderLayer
import net.minecraft.util.math.BlockPos
import net.minecraft.world.IBlockAccess
import net.minecraftforge.client.MinecraftForgeClient
import org.lwjgl.opengl.GL11


class GlowSimpleRenderer extends IBlockRenderer {
  override def renderBlock(world: IBlockAccess, pos: BlockPos, state: IBlockState, buffer: BufferBuilder): Boolean = {
    val layer = MinecraftForgeClient.getRenderLayer()
    val block = state.getBlock.asInstanceOf[IAnimationHandler]
    val renderState = CCRenderState.instance()
    layer match {
      case BlockRenderLayer.CUTOUT_MIPPED => {
        val textureModel = GlowSimpleRenderer.BLOCK_MODEL.copy()
        val parent = BakingVertexBuffer.create()
        parent.begin(GL11.GL_QUADS, DefaultVertexFormats.ITEM)
        renderState.reset()
        renderState.bind(parent)
        textureModel.setColour(block.getColorMultiplier(world, state, pos, 0))
        textureModel.render(renderState, new IconTransformation(block.getOverlayTexture(world, state, pos, 0)))
        parent.finishDrawing()
        RenderUtils.renderQuads(buffer, world, pos, parent.bake())
      }
      case _ => {
        val modelAnimation = GlowSimpleRenderer.BLOCK_MODEL.copy()
        modelAnimation.apply(new Translation(Vector3.fromBlockPos(pos)))
        renderState.reset()
        renderState.bind(buffer)
        modelAnimation.setColour(block.getAnimationColor(world, state, pos, 0))
        renderState.brightness = block.getAnimationBrightness(world, state, pos, 0)
        modelAnimation.render(renderState, new IconTransformation(block.getAnimationTexture(world, state, pos, 0)))
      }
    }
    return true
  }


  override def renderDamage(world: IBlockAccess, pos: BlockPos, buffer: BufferBuilder, texture: TextureAtlasSprite): Unit = {
  }

  override def renderInventory(stack: ItemStack, buffer: BufferBuilder): Unit = {
    val lastBrightness = OpenGlHelper.lastBrightnessY.toInt << 16 | OpenGlHelper.lastBrightnessX.toInt
    Tessellator.getInstance().draw()
    GlStateManager.pushMatrix()
    GlStateManager.disableLighting()
    val mipmapFilterData = RenderUtils.disableMipmap()
    buffer.begin(GL11.GL_QUADS, RenderUtils.getFormatWithLightMap(DefaultVertexFormats.ITEM))
    val renderState = CCRenderState.instance()
    val animationHandler = Block.getBlockFromItem(stack.getItem()).asInstanceOf[IAnimationHandler]
    renderState.reset()
    renderState.bind(buffer)
    val modelAnimation = GlowSimpleRenderer.BLOCK_MODEL.copy()

    val texture = animationHandler.getAnimationTexture(stack, 0)
    val color = animationHandler.getAnimationColor(stack, 0)
    val brightness = animationHandler.getAnimationBrightness(stack, 0)
    renderState.brightness = brightness
    modelAnimation.setColour(color)
    modelAnimation.render(renderState, new IconTransformation(texture))

    Tessellator.getInstance().draw()
    RenderUtils.enableMipmap(mipmapFilterData)
    GlStateManager.enableLighting()
    GlStateManager.popMatrix()
    buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.ITEM)
    renderState.reset()
    renderState.bind(buffer)
    renderState.brightness = lastBrightness

    val modelOverlay = GlowSimpleRenderer.BLOCK_MODEL.copy()
    modelOverlay.setColour(animationHandler.getColorMultiplier(stack, 0))
    val texture_overlay = animationHandler.getOverlayTexture(stack, 0)

    modelOverlay.render(renderState, new IconTransformation(texture_overlay))

    Tessellator.getInstance().draw()
    buffer.begin(GL11.GL_QUADS, RenderUtils.getFormatWithLightMap(DefaultVertexFormats.ITEM))
  }
}

object GlowSimpleRenderer {
  val BLOCK_MODEL = CCModel.quadModel(24).generateBlock(0, new Cuboid6(0.0, 0.0, 0.0, 1.0, 1.0, 1.0)).computeNormals()
}
