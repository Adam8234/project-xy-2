package com.projectxy.client;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;

public interface IBlockRenderer {
    boolean renderBlock(IBlockAccess world, BlockPos pos, IBlockState state, BufferBuilder buffer);

    void renderDamage(IBlockAccess world, BlockPos pos, BufferBuilder buffer, TextureAtlasSprite texture);

    void renderInventory(ItemStack stack, BufferBuilder buffer);
}
