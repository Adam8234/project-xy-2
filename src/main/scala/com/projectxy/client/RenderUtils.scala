package com.projectxy.client

import net.minecraft.block.state.IBlockState
import net.minecraft.client.Minecraft
import net.minecraft.client.renderer.BufferBuilder
import net.minecraft.client.renderer.block.model.{BakedQuad, IBakedModel, ItemCameraTransforms, ItemOverrideList}
import net.minecraft.client.renderer.texture.TextureAtlasSprite
import net.minecraft.client.renderer.vertex.{DefaultVertexFormats, VertexFormat}
import net.minecraft.util.EnumFacing
import net.minecraft.util.math.{BlockPos, MathHelper, Vec3i}
import net.minecraft.world.IBlockAccess
import net.minecraftforge.common.ForgeModContainer
import net.minecraftforge.fml.client.FMLClientHandler
import org.lwjgl.opengl.GL11


object RenderUtils {
  private val itemFormatWithLightMap = new VertexFormat(DefaultVertexFormats.ITEM).addElement(DefaultVertexFormats.TEX_2S)

  def disableMipmap(): MipmapFilterData = {
    val minFilter = GL11.glGetTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER)
    val magFilter = GL11.glGetTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER)
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST)
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST)
    val data = new MipmapFilterData()
    data.minFilter = minFilter
    data.magFilter = magFilter
    return data
  }

  def enableMipmap(data: MipmapFilterData): Unit = {
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, data.minFilter)
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, data.magFilter)
  }

  def renderQuads(buffer: BufferBuilder, world: IBlockAccess, pos: BlockPos, quads: java.util.List[BakedQuad]): Boolean = {
    val useAo = Minecraft.isAmbientOcclusionEnabled()
    val state = world.getBlockState(pos).getActualState(world, pos)
    val model = new BakedModelAdapter(quads)
    val bmr = Minecraft.getMinecraft().getBlockRendererDispatcher.getBlockModelRenderer
    val random = MathHelper.getPositionRandom(new Vec3i(pos.getX, pos.getY, pos.getZ))

    if (useAo) {
      bmr.renderModelSmooth(world, model, state, pos, buffer, true, random)
    } else {
      bmr.renderModelFlat(world, model, state, pos, buffer, true, random)
    }
  }

  def getFormatWithLightMap(format: VertexFormat): VertexFormat = {
    if (FMLClientHandler.instance().hasOptifine() || !ForgeModContainer.forgeLightPipelineEnabled) {
      return format
    }

    var result: VertexFormat = format

    if (format == DefaultVertexFormats.BLOCK) {
      result = DefaultVertexFormats.BLOCK
    } else if (format == DefaultVertexFormats.ITEM) {
      result = itemFormatWithLightMap
    } else if (!format.hasUvOffset(1)) {
      result = new VertexFormat(format)
      result.addElement(DefaultVertexFormats.TEX_2S)
    } else {
      result = format
    }

    result
  }

}

class BakedModelAdapter(private val quads: java.util.List[BakedQuad]) extends IBakedModel {

  override def getQuads(state: IBlockState, side: EnumFacing, rand: Long): java.util.List[BakedQuad] = quads

  override def isAmbientOcclusion(): Boolean = false

  override def isGui3d(): Boolean = false

  override def isBuiltInRenderer(): Boolean = false

  override def getParticleTexture(): TextureAtlasSprite = return null

  override def getItemCameraTransforms(): ItemCameraTransforms = ItemCameraTransforms.DEFAULT

  override def getOverrides(): ItemOverrideList = ItemOverrideList.NONE
}

class MipmapFilterData {

  var minFilter: Int = 0
  var magFilter: Int = 0

}
