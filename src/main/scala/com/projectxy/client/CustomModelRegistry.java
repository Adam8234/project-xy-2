package com.projectxy.client;

import codechicken.lib.model.DummyBakedModel;
import codechicken.lib.model.ModelRegistryHelper;
import com.projectxy.Blocks;
import com.projectxy.Blocks$;
import com.projectxy.block.BlockGlow;
import net.minecraft.block.Block;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMap;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;

import java.util.Collection;

public class CustomModelRegistry {
    public static final BlockRenderingImpl glowSimpleRenderer = new BlockRenderingImpl(new GlowSimpleRenderer());

    public static void registerGlowBlock(Block glowBlock) {
        ModelResourceLocation location = new ModelResourceLocation(glowBlock.getRegistryName().toString());
        ModelResourceLocation inventoryLocation = new ModelResourceLocation(glowBlock.getRegistryName(), "inventory");
        ModelLoader.setCustomStateMapper(glowBlock, buildStateMap(glowBlock));

        //Register all model's for each meta-data
        if (glowBlock instanceof BlockGlow) {
            for (int i = 0; i < ((BlockGlow) glowBlock).colors().length() - 1; i++) {
                ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(glowBlock), i, inventoryLocation);
            }
        }

        //Shut Minecraft the fuck up
        ModelRegistryHelper.register(location, new DummyBakedModel());
        ModelRegistryHelper.registerItemRenderer(Item.getItemFromBlock(glowBlock), glowSimpleRenderer);
    }

    private static StateMap buildStateMap(Block block) {
        StateMap.Builder builder = new StateMap.Builder();
        BlockStateContainer container = block.getBlockState();

        Collection<IProperty<?>> properties = container.getProperties();
        if (!properties.isEmpty()) {
            for (IProperty<?> property : properties) {
                builder.ignore(property);
            }
        }
        return builder.build();
    }
}
