package com.projectxy

import codechicken.lib.colour.{Colour, ColourRGBA, EnumColour}
import com.projectxy.block._
import com.projectxy.block.item.ItemBlockMetaHandler
import net.minecraft.block.Block
import net.minecraft.item.Item
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.event.RegistryEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.registries.IForgeRegistry

object Blocks {
  val mcColors = EnumColour.values().map((colour: EnumColour) => colour.getColour).toList
  val xyColors = List(new ColourRGBA(0, 100, 255, 255), new ColourRGBA(16711935), new ColourRGBA(-16776961), new ColourRGBA(30, 30, 30, 255), new ColourRGBA(-1))


  lazy val blockStone: BlockStone = new BlockStone()
  lazy val blockOre: BlockGlow = new BlockOre()
  lazy val blockStorage: BlockGlow = new BlockGlowTexture("storage", xyColors)

  //Bricks
  lazy val blockBrick: BlockGlow = new BlockGlowTexture("brick", xyColors) with IAnimationColorMultiplier
  lazy val blockPlate: BlockGlow = new BlockGlowTexture("plate", xyColors)
  lazy val blockPlatform: BlockGlow = new BlockGlowTexture("platform", xyColors)
  lazy val blockShield: BlockGlow = new BlockGlowTexture("shield", xyColors)

  lazy val lightDO: BlockXyLight = new BlockXyLight()
  lazy val blockCorn: BlockCorn = new BlockCorn()

  private def registerAllBlocks(registry: IForgeRegistry[Block]): Unit = {
    registry.register(blockStone)
    registry.registerAll(blockOre, blockStorage)
    registry.registerAll(blockPlate, blockPlatform, blockShield)
    registry.registerAll(blockBrick)
    registry.registerAll(blockCorn)
  }

  private def registerAllItemBlocks(registry: IForgeRegistry[Item]): Unit = {
    registry.register(new ItemBlockMetaHandler(blockStone))
    registry.registerAll(new ItemBlockMetaHandler(blockOre), new ItemBlockMetaHandler(blockStorage))
    registry.registerAll(new ItemBlockMetaHandler(blockBrick))
    registry.registerAll(new ItemBlockMetaHandler(blockPlate), new ItemBlockMetaHandler(blockPlatform), new ItemBlockMetaHandler(blockShield))
  }

  @SubscribeEvent
  def registerBlocks(event: RegistryEvent.Register[Block]): Unit = registerAllBlocks(event.getRegistry)
  @SubscribeEvent
  def registerItemBlocks(event: RegistryEvent.Register[Item]): Unit = registerAllItemBlocks(event.getRegistry)
  def init(): Unit = MinecraftForge.EVENT_BUS.register(this)
}
