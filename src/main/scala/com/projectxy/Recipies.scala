package com.projectxy

import java.lang.{Character => JChar}

import com.projectxy.block.BlockStone
import net.minecraft.item.ItemStack
import net.minecraft.item.crafting.CraftingManager
import net.minecraftforge.fml.common.registry.GameRegistry

object Recipies {
  def init(): Unit = {
    addBrickRecipie(output = BlockStone.Type.STONE_BRICK.toItemStack, input = BlockStone.Type.STONE.toItemStack)
    addBrickRecipie(output = BlockStone.Type.STONE_BRICK_FANCY.toItemStack, input = BlockStone.Type.STONE_BRICK.toItemStack)
    addBrickRecipie(output = BlockStone.Type.STONE_WHITE_BRICK.toItemStack, input = BlockStone.Type.STONE_WHITE.toItemStack)
    addBrickRecipie(output = BlockStone.Type.STONE_WHITE_BRICK_FANCY.toItemStack, input = BlockStone.Type.STONE_WHITE_BRICK.toItemStack)

    //Reverse White Stone Brick
    //GameRegistry.addRecipe(new ItemStack(Blocks.blockStone, 1, 3), "x", 'x': JChar, new ItemStack(Blocks.blockStone, 1, 4))
    //GameRegistry.addRecipe(new ItemStack(Blocks.blockStone, 1, 4), "x", 'x': JChar, new ItemStack(Blocks.blockStone, 1, 5))
    //Reverse Stone Brick
    //GameRegistry.addRecipe(new ItemStack(Blocks.blockStone, 1, 0), "x", 'x': JChar, new ItemStack(Blocks.blockStone, 1, 1))
    //GameRegistry.addRecipe(new ItemStack(Blocks.blockStone, 1, 1), "x", 'x': JChar, new ItemStack(Blocks.blockStone, 1, 2))
    for (index <- 0 to 4) {
      GameRegistry.addSmelting(new ItemStack(Items.itemXychorium, 1, index), new ItemStack(Items.itemXychoridite, 1, index), 4)
    }
  }

  def addBrickRecipie(input: ItemStack, output: ItemStack): Unit = {
    output.setCount(4)
    //GameRegistry.addRecipe(output, "xx", "xx", 'x': JChar, input)
  }
}
