package com.projectxy.tile

import codechicken.lib.colour.{Colour, ColourRGBA}
import codechicken.lib.packet.PacketCustom
import com.projectxy.ProjectXY
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.network.NetworkManager
import net.minecraft.network.play.server.SPacketUpdateTileEntity
import net.minecraft.tileentity.TileEntity

class TileXyLight extends TileEntity {
  private var color: Int = new ColourRGBA(255, 255, 255, 255).rgba()

  def setColor(r: Int, g: Int, b: Int): Unit = {
    setColor(new ColourRGBA(r, g, b, 255))
  }
  def setColor(colour: Colour): Unit = {
    color = colour.rgba()
    markDirty()
  }
  def getColor: Int = color
  def getColour: ColourRGBA = new ColourRGBA(color)

  override def markDirty(): Unit = {
    super.markDirty()
    if (getWorld != null) {
      val state = getWorld.getBlockState(getPos)
      if (state != null) {
        state.getBlock.updateTick(world, pos, state, getWorld.rand)
        getWorld.notifyBlockUpdate(getPos, state, state, 3)
      }
    }
  }

  override def writeToNBT(compound: NBTTagCompound): NBTTagCompound = {
    super.writeToNBT(compound)
    println(getColour)
    compound.setInteger("color", color)
    return compound
  }

  def sendUpdatePacket(): Unit = {
    val packet = new PacketCustom(ProjectXY.MOD_ID, 1)
    packet.writePos(getPos)
    packet.sendToClients()
  }

  def handleUpdatePacket(packetCustom: PacketCustom): Unit = {
    markDirty()
  }

  override def getUpdatePacket: SPacketUpdateTileEntity = new SPacketUpdateTileEntity(pos, 0, getUpdateTag)
  override def getUpdateTag: NBTTagCompound = writeToNBT(super.getUpdateTag)

  override def onDataPacket(net: NetworkManager, pkt: SPacketUpdateTileEntity): Unit = {
    readFromNBT(pkt.getNbtCompound)
  }

  override def readFromNBT(compound: NBTTagCompound): Unit = {
    super.readFromNBT(compound)
    color = compound.getInteger("color")
    return compound
  }
}
