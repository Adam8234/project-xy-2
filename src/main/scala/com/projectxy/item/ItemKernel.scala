package com.projectxy.item

import com.projectxy.Blocks
import net.minecraft.block.state.IBlockState
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.math.BlockPos
import net.minecraft.util.{EnumActionResult, EnumFacing, EnumHand}
import net.minecraft.world.{IBlockAccess, World}
import net.minecraftforge.common.{EnumPlantType, IPlantable}

class ItemKernel extends ItemXy("corn_kernel") with IPlantable {
  override def getPlant(world: IBlockAccess, pos: BlockPos): IBlockState = Blocks.blockCorn.withAge(0)

  override def getPlantType(world: IBlockAccess, pos: BlockPos): EnumPlantType = EnumPlantType.Crop

  override def onItemUse(player: EntityPlayer, worldIn: World, pos: BlockPos, hand: EnumHand, facing: EnumFacing, hitX: Float, hitY: Float, hitZ: Float): EnumActionResult = {
    if (facing != EnumFacing.UP) {
      return EnumActionResult.FAIL
    }
    val heldItem = player.getHeldItem(hand)
    if (player.canPlayerEdit(pos, facing, heldItem)
      && player.canPlayerEdit(pos.up(), facing, heldItem)
      && player.canPlayerEdit(pos.up(2), facing, heldItem)) {
      val groundState = worldIn.getBlockState(pos)
      if (groundState.getBlock.canSustainPlant(groundState, worldIn, pos, facing, this) && worldIn.isAirBlock(pos.up())) {
        worldIn.setBlockState(pos.up(), Blocks.blockCorn.withAge(0))
        heldItem.shrink(1)
        return EnumActionResult.SUCCESS
      }
    }
    EnumActionResult.FAIL
  }
}
