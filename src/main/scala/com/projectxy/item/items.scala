package com.projectxy.item

import codechicken.lib.colour.Colour
import com.projectxy.ProjectXYBlocksTab
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.{Item, ItemFood, ItemStack}
import net.minecraft.util.NonNullList

trait TColoredItem extends Item {
  setHasSubtypes(true)

  val colors: List[Colour]

  def colorMultiplier(stack: ItemStack): Colour = colors(stack.getMetadata)

  override def getSubItems(tab: CreativeTabs, items: NonNullList[ItemStack]): Unit = {
    if (getCreativeTab == tab) {
      for (meta <- colors.indices) {
        items.add(new ItemStack(this, 1, meta))
      }
    }
  }
}

class ItemXy(name: String) extends Item {
  setRegistryName(name)
  setUnlocalizedName(name)
  setCreativeTab(ProjectXYBlocksTab)
}

class ItemXyFood(name: String, amount: Int) extends ItemFood(amount, false) {
  setRegistryName(name)
  setUnlocalizedName(name)
  setCreativeTab(ProjectXYBlocksTab)
}

class ItemXychoridite(override val colors: List[Colour]) extends Item with TColoredItem {
  setRegistryName("xychoridite")
  setUnlocalizedName("itemXychoridite")
  setCreativeTab(ProjectXYBlocksTab)
}

class ItemXychorium(override val colors: List[Colour]) extends Item with TColoredItem {
  setRegistryName("xychorium")
  setUnlocalizedName("itemXychorium")
  setCreativeTab(ProjectXYBlocksTab)
}
