package com.projectxy

import codechicken.lib.packet.PacketCustom
import codechicken.lib.render.block.BlockRenderingRegistry
import com.projectxy.block.BlockStone
import com.projectxy.client.CustomModelRegistry
import com.projectxy.item.TColoredItem
import net.minecraft.client.renderer.block.model.ModelResourceLocation
import net.minecraft.item.Item
import net.minecraftforge.client.event.ModelRegistryEvent
import net.minecraftforge.client.model.ModelLoader
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent

/**
  * Created by Acorp on 7/1/2017.
  */
object ModelHandler {
  def init(): Unit ={
    MinecraftForge.EVENT_BUS.register(this)
  }

  @SubscribeEvent
  def registerModels(event: ModelRegistryEvent): Unit = {
    BlockStone.Type.values().toList.zipWithIndex.foreach((tuple: (BlockStone.Type, Int)) => {
      ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(Blocks.blockStone),
        tuple._2,
        new ModelResourceLocation(ProjectXY.MOD_ID + ":stone", "type=" + BlockStone.Type.values.apply(tuple._2).getName))
    })

    CustomModelRegistry.registerGlowBlock(Blocks.blockOre)
    CustomModelRegistry.registerGlowBlock(Blocks.blockStorage)
    CustomModelRegistry.registerGlowBlock(Blocks.blockBrick)
    CustomModelRegistry.registerGlowBlock(Blocks.blockPlate)
    CustomModelRegistry.registerGlowBlock(Blocks.blockPlatform)
    CustomModelRegistry.registerGlowBlock(Blocks.blockShield)

    //CustomModelRegistry.registerGlowBlock(Blocks.lightDO)

    registerTColoredItem(Items.itemXychoridite)
    registerTColoredItem(Items.itemXychorium)

    ModelLoader.setCustomModelResourceLocation(Items.itemCorn, 0, new ModelResourceLocation(Items.itemCorn.getRegistryName, "inventory"))
    ModelLoader.setCustomModelResourceLocation(Items.itemCornCob, 0, new ModelResourceLocation(Items.itemCornCob.getRegistryName, "inventory"))
    ModelLoader.setCustomModelResourceLocation(Items.itemPopcorn, 0, new ModelResourceLocation(Items.itemPopcorn.getRegistryName, "inventory"))
    ModelLoader.setCustomModelResourceLocation(Items.itemCornKernel, 0, new ModelResourceLocation(Items.itemCornKernel.getRegistryName, "inventory"))

    PacketCustom.assignHandler(ProjectXY.MOD_ID, ProjectXYCPH)
  }

  private def registerTColoredItem(tColoredItem: TColoredItem): Unit = {
    for (meta <- tColoredItem.colors.indices) {
      ModelLoader.setCustomModelResourceLocation(tColoredItem, meta, new ModelResourceLocation(tColoredItem.getRegistryName, "inventory"))
    }
  }
}
